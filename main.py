import os, requests

from flask import Flask

app = Flask(__name__)


@app.route("/")
def hello_world():
    name = os.environ.get("NAME", "World")
    return "Hello {}!".format(name)

@app.route("/service1")
def get_request():
    headers = {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            }
    response = requests.get( f'https://cloudruntest-ny5xbtjf4q-uc.a.run.app/', headers)
    print(response)
    return response 

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))

